from PIL import Image
import time

IMAGE = Image.open('test.png')

cdef int PIXEL_COUNT, WIDTH, HEIGHT
PIXEL_COUNT = IMAGE.size[0] * IMAGE.size[1]
WIDTH = IMAGE.size[0]
HEIGHT = IMAGE.size[1]
RANGE_X = xrange(WIDTH)
RANGE_Y = xrange(HEIGHT)

IMG_DATA = IMAGE.load()

# write every single color value into a three-dimensional array
cdef int IMG_C[1366][768][3]
cdef int i, x, y
for x in RANGE_X:
    for y in RANGE_Y:
        for i in range(0, 3):
            IMG_C[x][y][i] = IMG_DATA[x, y][i]

def colorAverage (int PIXEL_COUNT):
    global IMG_C
    cdef int r, g, b, x, y
    r, g, b = 0, 0, 0
    for x in RANGE_X:
        for y in RANGE_Y:
            r += IMG_C[x][y][0]
            g += IMG_C[x][y][1]
            b += IMG_C[x][y][2]
    return r/PIXEL_COUNT, g/PIXEL_COUNT, b/PIXEL_COUNT

DURATION_SUM = 0
for i in xrange(10):
    start = time.time()
    COLOR_AVG = colorAverage(PIXEL_COUNT)
    end = time.time()

    duration = end - start
    print round(duration, 5), "seconds"

    DURATION_SUM += duration
DURATION_AVG = DURATION_SUM / 10

print "average color: rgb(%s, %s, %s)" % (COLOR_AVG)
print "image size: %s pixel" % PIXEL_COUNT
print "average duration: %s seconds" % round(DURATION_AVG, 5)