all:
	python setup.py build_ext --inplace

clean:
	rm *.so &
	rm *.c &
	rm *.pyc &
	rm -rf build/ &
