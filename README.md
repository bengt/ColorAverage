##  Run as Interpreted Python Script

    $ python ColorAverage.py
    1.2613 seconds
    1.2547 seconds
    1.2567 seconds
    1.2564 seconds
    1.2624 seconds
    1.2552 seconds
    1.2727 seconds
    1.2545 seconds
    1.2549 seconds
    1.2633 seconds
    average color: rgb(146, 133, 99)
    image size: 1049088 pixel
    average duration: 1.2592 seconds

##  Build Python Extension

    make

##  Run as Compiled Python Extension

    $ python -c "import ColorAverage"
    0.03454 seconds
    0.03471 seconds
    0.03468 seconds
    0.03484 seconds
    0.03483 seconds
    0.03481 seconds
    0.03478 seconds
    0.03477 seconds
    0.03471 seconds
    0.03465 seconds
    average color: rgb(146, 133, 99)
    image size: 1049088 pixel
    average duration: 0.03473 seconds
