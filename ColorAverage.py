from PIL import Image
import time

IMAGE = Image.open('test.png')
PIXEL_COUNT = IMAGE.size[0] * IMAGE.size[1]

IMG_DATA = IMAGE.load()
RANGE_Y = xrange(IMAGE.size[0])
RANGE_X = xrange(IMAGE.size[1])
IMG_DATA = [[IMG_DATA[y, x] for y in RANGE_Y] for x in RANGE_X]

def colorAverage ():
    r, g, b = 0, 0, 0
    for x in RANGE_X:
        for y in RANGE_Y:
            temp = IMG_DATA[x][y]
            r += temp[0]
            g += temp[1]
            b += temp[2]
    return r/PIXEL_COUNT, g/PIXEL_COUNT, b/PIXEL_COUNT

DURATION_SUM = 0
for i in range(0, 10):
    start = time.time()
    COLOR_AVG = colorAverage()
    end = time.time()

    duration = end - start
    print round(duration, 4), "seconds"

    DURATION_SUM += duration
DURATION_AVG = DURATION_SUM / 10

print "average color: rgb(%s, %s, %s)" % (COLOR_AVG)
print "image size: %s pixel" % PIXEL_COUNT
print "average duration: %s seconds" % round(DURATION_AVG, 4)